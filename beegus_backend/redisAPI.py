import redis
import hug
import falcon
from datetime import datetime
from falcon import HTTPError, HTTP_400



"""
Queue: {
 name: "",
 song_list: [
      { 
	title: str,
	artist: str,
	album: str,
	who added: str,
	album art: URL,
	upvotes: int,
	downvotes: int,
	progress: int (MS through song),
	duration: int (MS song length),
        time added: datetime,
      }
 ...]
 

}
"""

def validate_queue(obj):
    return obj is not None


#def get_new_uuid():
#    return rdb.incr('uuid_counter')


def retrieve_queue(name):
    redis_val = rdb.get(name)
    if redis_val is not None:
        return eval(redis_val.decode())
    else:
        return None


@hug.get('/queue/{name}')
def get_queue(name):
    queue = retrieve_queue(name)
    if validate_queue(queue):
        return queue
    else:
        raise HTTPError(HTTP_400)


@hug.post('/create_queue')
def create_queue(name, response=None):
    queue_dict =  {
            'name': name,
            'song_list' : []
            }
    rdb.set(name, queue_dict)
    response.status = falcon.HTTP_201
    response.content_location = f"/queue/{name}"

@hug.post('/set_queue')
def set_queue(body: hug.types.json, response=None):
    q = body['queue']
    if validate_queue(q):
        rdb.set(q['name'], q)
        response.status = falcon.HTTP_201
        response.content_location = f"/queue/{q['name']}"
    else:
        raise HTTPError(HTTP_400)
    

@hug.delete('/delete_queue/{name}')
def delete_queue(name, response=None):
    rdb.delete(name)
    response.status = falcon.HTTP_204


@dataclass
class Song():
    name: str
    artist: str
    album: str
    length: int
    uri: str
    album_art: str


@dataclass
class Queue():
    queueuid: str
    owner_name: str
    number_of_guests: int
    time_created: datetime
    active: bool



class Redis_inst():
    def __init__():
        self.rdb = redis.Redis.from_url('redis://localhost:1234')
    
    def create_queue(queueuid):
        pass

    def get_queue(queueuid):
        pass

    def remove_queue(queueuid):
        pass

    def get_top_song(queueid):
        pass

    def pop_top_song(queueid):
        pass

    def add_guest(queueid):
        pass

    def remove_guest(queueuid):
        pass

    def add_song_metadata(song: Song):
        pass

    def add_song(queueuid, uri):
        pass

    def remove_song(queueuid, uri):
        pass

    def upvote_song(queueuid, uri):
        pass

    def downvote_song(queueid, uri):
        pass

    

