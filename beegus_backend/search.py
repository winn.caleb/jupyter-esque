import requests
import pickle
import json

tokens = pickle.load(open('tokens.pik', 'rb'))
response = requests.get(
        "https://api.spotify.com/v1/me/playlists",
        headers= {"Authorization" : "Bearer " + tokens['access_token']}
        )
uri = ""
for pl in response.json()['items']:
    if pl['name'] == 'Dolce vita':
        print('it worked!')
        uri = pl['uri']

print(uri)
response = requests.put(
        'https://api.spotify.com/v1/me/player/play',
        data = json.dumps({'context_uri': uri, 
                            'position_ms': 0}),
        headers= {"Authorization" : "Bearer " + tokens['access_token']}
        )
