A list of the APIs, their usages, arguments, and who they will be exposed to. For the sake of concision, some arguments are listed as common.

Common
 "Arguments extant in all APIS"
 - INARGS:
	Oauthtoken: hash for authentication that identifies user and their queue


THE QUEUE OBJECT:
[ {
	title: str
	artist: str
	album: str
	who added: str
	album art: URL
	upvotes: int
	downvotes: int
	progress: int (MS through song)
	duration: int (MS song length)
  }
...] 


Search
 "Query Spotify library for songs"
  GET
  - INARGS:
	query (str): string from the search bar
  - OARGS:
	results (json): list of results objects including the following
		 - title
		 - artist
		 - album
		 - album art
		 - URI
  - ACCESS:
	master and sub


PLAY PAUSE:
 "Toggle playing music"
 POST
  - INARGS:
	I don't think we need any
  - OUTARGS:
	play status (bool): true if playing false if paused
  - ACCESS:
	master


SKIP
 "Move to the next song"
 POST
 - INARGS:
	num: INT num songs skipped
 - OARGS:
	Queue: updated list


ADD TO QUEUE:
 "Put a song in the queue"
 POST
  - INARGS:
	URI: ID of the song
  - OARGS:
    Queue: updated list
	
  - ACCESS:
	master and sub


GET QUEUE

GET AVAILABLE DEVICES

UPDATE QUEUE

TERMINATE SESSION




 

