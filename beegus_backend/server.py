import hug
import requests
from spotify import client_id, client_secret
import base64
import pickle
import json

@hug.get('/callback')
def callback(request, body):
    if "code" in request.params and "state" in request.params:
        response = requests.post(
                "https://accounts.spotify.com/api/token",
                data = {
                    "grant_type": "authorization_code",
                    "code": request.params['code'],
                    'redirect_uri': 'http://localhost:8000/callback',
                    },
                auth = (client_id, client_secret)
                )
        tokens = response.json()
        pickle.dump(tokens, open('tokens.pik','wb'))
        response = requests.get(
                "https://api.spotify.com/v1/me/playlists",
                headers= {"Authorization" : "Bearer " + tokens['access_token']}
                )
        uri = ""
        for pl in response.json()['items']:
            if pl['name'] == 'Dolce vita':
                print('it worked!')
                uri = pl['uri']

        print(uri)
        response = requests.put(
                'https://api.spotify.com/v1/me/player/play',
                data = json.dumps({'context_uri': uri, 
                                    'position_ms': 208300}),
                headers= {"Authorization" : "Bearer " + tokens['access_token']}
                )
        return response.json()
    else:
        print(request)
        return {"this is a thing": "thing"}
