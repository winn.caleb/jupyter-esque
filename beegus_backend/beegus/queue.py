import json
import falcon
import msgpack
import redis
import uuid

rdb = redis.Redis.from_url('redis://localhost:1234')

class QueueResource:

    def __init__(self, uuidgen=uuid.uuid1):
        self._uuidgen = uuidgen
    
    def retrieve_queue(r_id):
        redis_val = rdb.hget(r_id)
        if redis_val is not None:
            return eval(redis_val.decode())
        else:
            return None

    def on_get(self, req, resp, queue_id):

        queue = retrieve_queue(queue_id)
    
        if queue:
            # MSGPACK representation of the body
            # resp.data = msgpack.packb(test_queue, use_bin_type=True)
            # resp.content_type = falcon.MEDIA_MSGPACK

            # JSON representation of the body
            resp.body = json.dumps(queue, ensure_ascii=False)
            resp.content_type = falcon.MEDIA_JSON
            
            resp.status = falcon.HTTP_200
        else:
            resp.status = falcon.HTTP_404

    def on_post(self, req, resp):
        """ posts can update the queue, there should be a list of allowed
        actions that update the queue in certain ways
        """
        post_dict = json.loads(req.stream.read())

        #require name
        name = post_dict.get('name')
        action = post_dict.get('action', None)
        queue_id = post_dict.get('id', str(self._uuidgen()))

        updated_queue = {
                'name': name,
                'song_list': []
                }

        # can't save dicts to redis
        rdb.set(queue_id, json.dumps(updated_queue))

        resp.body = json.dumps(updated_queue, ensure_ascii=False)
        resp.content_type = falcon.MEDIA_JSON
        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp, queue_id):
        if name == 'test':
            test_queue = {
                    'name': 'test',
                    'song_list': []
                    }

            # MSGPACK representation of the body
            # resp.data = msgpack.packb(test_queue, use_bin_type=True)
            # resp.content_type = falcon.MEDIA_MSGPACK

            # JSON representation of the body
            resp.body = json.dumps(test_queue, ensure_ascii=False)
            resp.content_type = falcon.MEDIA_JSON
            
            resp.status = falcon.HTTP_200
        else:
            resp.status = falcon.HTTP_404




