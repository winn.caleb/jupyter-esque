
var audio = document.getElementById('audio');
var playpause = document.getElementById("play");
var magicButton = document.getElementById('magicButton')
var magicButton2 = document.getElementById('magicButton2')


function togglePlayPause() {
   if (audio.paused || audio.ended) {
      playpause.title = "Pause";
      audio.play();
   } else {
      playpause.title = "Play";
      audio.pause();
   }
}

document.getElementById("queue-icon").addEventListener("click", function(){
  magicButton.checked = !magicButton.checked;
});

document.getElementById("playTab").addEventListener("click", function(){
  magicButton.checked = false;
  magicButton2.checked = false;
});

document.getElementById("queueTab").addEventListener("click", function(){
  magicButton.checked = true;
  magicButton2.checked = false;
});

document.getElementById("searchTab").addEventListener("click", function(){
  magicButton2.checked = true;
});
